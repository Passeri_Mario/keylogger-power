# Keylogger Power

Malicious Keylogger application in PHP and JS to recover keyboard keys.

## Getting started

## 1 download the repository

`git clone https://gitlab.com/Passeri_Mario/keylogger-power.git`

## 2 Configure application

Creation of the index.php file exposing the malicious payload.

`php add-service.php nom_du_service`

Service available [facebook, naturabuy, default]

## optional

Command for help

`php help.php help`

## Concept

**Code written during my cybersecurity study phase in order to learn how to create a keylogger concept and configurable on the command line.**
