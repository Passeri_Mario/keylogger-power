<?php
    if(htmlentities($argv[1]) === "help"){
        $commands = array(
            "\n\033[1;31mInfo\033[0m" => "Description\n",
            "\033[1;31mKeylogger Power\033[0m" => "Malicious Keylogger application in PHP and JS to recover keyboard keys.\n",
            "\033[1;31mphp add-service.php nom_du_service\033[0m" => "Configuring the application with a chosen name version of the service.\n",
            "\033[1;31mService names available\033[0m" => "[facebook, naturabuy]\n",
            "\033[1;31mphp help.php help\033[0m" => "Displays information as well as available commands of the application.\n",
            "\033[1;31mVersion\033[0m" => "1.0.0\n",
        );

        foreach($commands as $key => $value) {
            echo $key . ": " . $value . "\n";
        }
    }else{
        echo "The command \033[1;31m'$argv[1]'\033[0m does not exist.";
    }