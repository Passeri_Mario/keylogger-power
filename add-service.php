<?php
    $file = "index.php";
    $version = htmlentities($argv[1]);
    include "config/config.php";
    if (in_array($version, $service_available)) {
        $handle = fopen($file, 'wb');
        fwrite($handle, '');
        $content = "<?php \$version = '$version'; ?>\n";
        $content .= "<!DOCTYPE html>\n";
        $content .= "<html lang=\"fr\">\n";
        $content .= "    <head>\n";
        $content .= "        <meta charset=\"UTF-8\">\n";
        $content .= "        <title>Enr</title>\n";
        $content .= "    </head>\n";
        $content .= "    <body>\n";
        $content .= "        <?php\n";
        $content .= "            \$return_value = match(\$version) {\n";
        $content .= "                'facebook' => include \"templates/facebook/facebook.php\",\n";
        $content .= "                'naturabuy' => include \"templates/naturabuy/naturabuy.php\",\n";
        $content .= "                default => include \"templates/default/default.php\"\n";
        $content .= "            };\n";
        $content .= "        ?>\n";
        if($version === "facebook" || $version === "naturabuy"){
            $content .= "    <script src='app/app.js'></script>\n";
        }
        $content .= "    </body>\n";
        $content .= "</html>\n";
        fwrite($handle, $content);
        fclose($handle);
        echo "The file \033[1;31m$file\033[0m was successfully modified with the variable \$version set to '\033[1;31m$version\033[0m'.\n";
    } else{
        echo "The file \033[1;31m$file\033[0m has not been modified because the value of the parameter does not exist";
    }