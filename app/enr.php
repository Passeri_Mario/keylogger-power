<?php

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $touches = $_POST['touches'];
        $adresseIp = $_SERVER['REMOTE_ADDR'];
        $nomFichier = 'IP - ' . $adresseIp . '.txt';
        $cheminFichier = __DIR__ . '/../data/' . $nomFichier;

        $handle = fopen($cheminFichier, 'ab');
        fwrite($handle, $touches . "\n");
        fclose($handle);

        $response = array('success' => true);
    } else {
        $response = array('success' => false, 'message' => 'Méthode HTTP non valide');
    }

    echo json_encode($response);